# -*- coding: utf-8 -*-
"""
Created on Sat May 23 13:06:21 2020

@author: Gebruiker1
"""

import sqlite3

def voortgang(status, resterend, totaal):
    print(f'{resterend} records van {totaal} overgenomen...')

try:
    #existing DB
    huidigeDB = sqlite3.connect('IPBDAM4.db, timeout=10')
    #copy into this DB
    StagingAreaDB = sqlite3.connect('staging.db, timeout=10')
    with StagingAreaDB:
        huidigeDB.backup(StagingAreaDB, pages=100, progress=voortgang)
    print("Succesvol geladen naar Staging area")

except sqlite3.Error as error:
    print("Error tijdens het wegschrijven naar Staging area: ", error)

finally:
    if(StagingAreaDB):
        StagingAreaDB.close()
        huidigeDB.close()