import sqlite3

def progress(status, remaining, total):
    print(f'Copied {total-remaining} of {total} pages...')

try:
    #existing DB
    sqliteCon = sqlite3.connect('IPBDAM4.db, timeout=10')
    #copy into this DB
    backupCon = sqlite3.connect('staging.db, timeout=10')
    with backupCon:
        sqliteCon.backup(backupCon, pages=10, progress=progress)
    print("backup successful")

except sqlite3.Error as error:
    print("Error while taking backup: ", error)
