# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 12:35:19 2020

@author: menno
"""

import dash
import dash_core_components as dcc
import dash_html_components as html
import datetime
from dateutil.relativedelta import relativedelta
import plotly.graph_objs as go
import sqlite3
import pandas as pd



start = datetime.datetime.today() - relativedelta(years=5)
end = datetime.datetime.today()

conn = sqlite3.connect('IPBDAM4.db, timeout=10')
df = pd.read_sql_query("SELECT * FROM ripple order by time desc limit 24", conn)
df2 = pd.read_sql_query("SELECT * FROM aex", conn)

#create candlestick ripple
fig = go.Figure(data=[go.Candlestick(x=df['time'],
                open=df['open'], high=df['high'],
                low=df['low'], close=df['close'])
                     ], layout={'plot_bgcolor': '#282B33', 'paper_bgcolor':'#30333D', 'font':{'color': 'white'}})

ripple_latest = df['close'].iloc[0]
ripple_latest_date = df['time'].iloc[0]
aex_latest = df2['value'].iloc[-1]
aex_latest_date = df2['date'].iloc[-1]
df3 = pd.read_sql_query("SELECT * FROM nieuws", conn)
news_latest =  df3['body'].iloc[-1]

fig.update_layout(xaxis_rangeslider_visible=False)

#create dashboard
app = dash.Dash(__name__)

#layout dashboard
app.layout = html.Div([
#banner, voor some reason moet div styling inline gebeuren, anders functioneerd deze niet    
    html.Div([
        html.H1("Ripple Base"),
        html.Img(src="/assets/ripple-icon.png")
    ], className="banner", style={'height':'81px','background-color':'rgb(66, 196, 247)', 'margin': '0px -10px 0px 0px', 'border-radius':'2px'}),
    
#printen laatste ripple close waarde
    html.Div([
        html.H2("Latest ripple close"),
        html.H2(ripple_latest),
        html.H2(ripple_latest_date)
        ], style={'background-color':'#30333D', 'color':'white', 'display':'inline-block', 'padding':'2%', 'margin': '1.5% 0% 1.5% 1.5%'}),
    
#printen laatste aex close waarde
    html.Div([
        html.H2("Latest aex close"),
        html.H2(aex_latest),
        html.H2(aex_latest_date)
        ], style={'background-color':'#30333D', 'color':'white', 'display':'inline-block', 'padding':'2%', 'margin': '1.5% 0% 1.5% 1.5%'}),

#printen candlestick ripple, classes komen van externe css af, deze werkt momenteel niet.
    html.Div([
        html.Div([
            dcc.Graph(
                figure = fig
            ),
        ], className="six columns"),
    ],className="row", style={'margin':'0% 1.5%'}),
    
#printen van meest recente nieuws
        html.Div([
        html.H2("Latest news"),
        html.H2(news_latest),
        html.H2("Latest Update: " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), style={'opacity': '1', 'color':'white','margin-bottom': '0px', 'font-size': '14px'}),
        ],style={'background-color':'#30333D', 'color':'white', 'display':'inline-block', 'padding':'2%', 'margin': '1.5% 0% 1.5% 1.5%'}),
    
    app.css.append_css({
    "external_url":"https://codepen.io/chriddyp/pen/bWLwgP.css"
    })
    ], style={'background-color': '#1A1C23', 'margin':'-10px','overflow':'hidden'})


if __name__=="__main__":
    app.run_server(debug=False)
