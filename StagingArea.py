# -*- coding: utf-8 -*-
"""
Created on Fri May 15 11:05:17 2020

@author: Gebruiker1
"""

import sqlite3

conn = sqlite3.connect('staging.db, timeout=10')
c = conn.cursor()

def create_aex():
    c.execute('CREATE TABLE aex (date TEXT, value REAL)')
    conn.commit()
    
def create_ripple ():
    c.execute('CREATE TABLE ripple (time TEXT, high REAL, low REAL, open REAL, volumefrom REAL, volumeto REAL, close REAL, conversiontype TEXT, conversionsymbol TEXT)')
    conn.commit()
    
def create_nieuws():
    c.execute('CREATE TABLE nieuws (id INTEGER, guid TEXT, published_on INTEGER, imageurl TEXT, title TEXT, url TEXT, source TEXT, body TEXT, tags TEXT, categories TEXT, upvotes INTEGER, downvotes INTEGER, lang TEXT, published_on_nu TEXT)')
    conn.commit()

if __name__ == "__main__":
    create_aex()
    create_ripple()
    create_nieuws()