# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import sqlite3
import pandas as pd
import json
from pandas.io.json import json_normalize
from datetime import datetime
import datetime as dt
import bs4
import requests

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 3000)


conn = sqlite3.connect('IPBDAM4.db, timeout=10')
c = conn.cursor()

#data types moeten mogelijk aangepast worden omdat sqlite vaag is
def create_table():
    c.execute('CREATE TABLE nieuws (id INTEGER, guid TEXT, published_on INTEGER, imageurl TEXT, title TEXT, url TEXT, source TEXT, body TEXT, tags TEXT, categories TEXT, upvotes INTEGER, downvotes INTEGER, lang TEXT, published_on_nu TEXT)')
    conn.commit()


# =============================================================================
# def real_entry():
#     response = requests.get("https://min-api.cryptocompare.com/data/v2/histohour?fsym=XRP&tsym=USD&limit=1")
#     results = response.json()
#     
#     #normalize
#     df = pd.io.json.json_normalize(results)
#     df.columns = df.columns.map(lambda x: x.split(".")[-1])
#     data = json_normalize(data=results['Data'], record_path='Data', errors='ignore')
#     #converting time
#     data['time'] = pd.to_datetime(data['time'], unit='s')
#     data['time'] = data['time'].apply(lambda x: x + dt.timedelta(hours=2)) #timedelta calculate difference between x and y time.
#     data = data.iloc[1:]
#     print("data: --------- : ", data)
#     c.execute("INSERT INTO ripple VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", (str(data['time']), float(data['high']), float(data['low']), float(data['open']),float(data['volumefrom']), float(data['volumeto']), float(data['close']), str(data['conversionType']), str(data['conversionSymbol'])))
#     conn.commit()
# =============================================================================
    
#ophalen van ripple data
def test2():
    response = requests.get("https://min-api.cryptocompare.com/data/v2/histohour?fsym=XRP&tsym=USD&limit=1")
    results = response.json()
    
    #normalize
    df = pd.io.json.json_normalize(results)
    df.columns = df.columns.map(lambda x: x.split(".")[-1])
    data = json_normalize(data=results['Data'], record_path='Data', errors='ignore')
    #converting time
    data['time'] = pd.to_datetime(data['time'], unit='s')
    data['time'] = data['time'].apply(lambda x: x + dt.timedelta(hours=2)) #timedelta calculate difference between x and y time.
    data = data.iloc[1:]
    data.to_sql(name='ripple', con=conn, index=False, if_exists='append')
    conn.commit()

#webscrapen van aex data
def scrapen():
    r = requests.get('https://www.iex.nl/Index-Koers/12272/AEX.aspx')
    soup = bs4.BeautifulSoup(r.text, 'xml')
    a = soup.find('table', {'class': 'ContentTable SmallContentTable'}).find('span', {'id': "12272LastTime"}).text
    b = soup.find('table', {'class': 'ContentTable SmallContentTable'}).find('span', {'id': "12272LastPrice"}).text
    df = pd.DataFrame({'date': a, 'value': b}, index=[0])
    df.to_sql(name='aex', con=conn, index=False, if_exists='append')

#inserten van een row (gebruikt om te testen)
def entry():
    c.execute("INSERT INTO ripple VALUES('2016-08-09 19:00:00', 16.008, 16.008, 16.008, 16.008, 16.008, 16.008, 'direct', 'btc')" )
    conn.commit()

#see functies zijn allemaal voor het tonen van de table, dit was gebruikt voor makkelijk testen
def see_ripple():
    c.execute("SELECT * FROM ripple")
    conn.commit()
    rows = c.fetchall()
    for row in rows:
        print(row)
    
        
def see_aex():
    c.execute("SELECT * FROM aex")
    conn.commit()
    rows = c.fetchall()
    for row in rows:
        print(row)
 
#ophalen meest recente nieuws
def latest_nieuws():
    c.execute("SELECT published_on from nieuws ORDER BY published_on DESC LIMIT 1;")
    rows = c.fetchone()
    latest = rows[0]
    response = requests.get("https://min-api.cryptocompare.com/data/v2/news/?categories=XRP")
    results = response.json()
    data = json_normalize(results['Data'])
    data['published_on_nu'] = pd.to_datetime(data['published_on'], unit='s')
    data['published_on_nu'] = data['published_on_nu'].apply(lambda x: x + dt.timedelta(hours=2))
    data = data.drop("source_info.name", axis=1)
    data = data.drop("source_info.lang", axis=1)
    data = data.drop("source_info.img", axis=1)
    df = data[~(data['published_on'] <= latest)]
    df.to_sql(name='nieuws', con=conn, index=False, if_exists='append')
        
def see_nieuws():
    c.execute("SELECT * FROM nieuws")
    conn.commit()
    rows = c.fetchall()
    for row in rows:
        print(row)
        
def drop():
    c.execute("DROP TABLE ripple")

test2()
scrapen()
latest_nieuws()
c.close()
conn.close()

