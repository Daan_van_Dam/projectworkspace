# -*- coding: utf-8 -*-
"""
Created on Fri May 15 12:40:10 2020

@author: menno
"""


import sqlite3
from datetime import date
from datetime import datetime
conn = sqlite3.connect('staging.db')
c = conn.cursor()

def overzetten():
    c.execute("ATTACH DATABASE 'dwh.db' as dwh")
    c.execute("INSERT INTO dwh.aex ('value') SELECT value FROM aex")
    conn.commit()

def see():
    c.execute("select value from aex")
    
if __name__ == "__main__":
    overzetten()
