# -*- coding: utf-8 -*-
"""
Created on Sat May 23 10:26:37 2020

@author: menno
"""
import sqlite3

conn = sqlite3.connect('dwh.db')
c = conn.cursor()

def create_dwh():
    c.execute('CREATE TABLE date (idDate INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, begin_time TEXT, end_time TEXT)')
    c.execute('CREATE TABLE news (idNews INTEGER PRIMARY KEY AUTOINCREMENT, date_idDate INTEGER, news_title TEXT, news_source TEXT, news_body TEXT, news_categories TEXT, snapshot_added TEXT, snapshot_validity TEXT, FOREIGN KEY (date_idDate) REFERENCES date (idDate))')
    c.execute('CREATE TABLE aex (idAEX INTEGER PRIMARY KEY AUTOINCREMENT, date_idDate INTEGER, value REAL, snapshot_added DATETIME DEFAULT CURRENT_DATE, snapshot_validity TEXT, FOREIGN KEY (date_idDate) REFERENCES date (idDate))')
    c.execute('CREATE TABLE XRP (idRipple INTEGER PRIMARY KEY AUTOINCREMENT, date_idDate INTEGER, xrp_high REAL, xrp_low REAL, xrp_open REAL, xrp_close REAL, xrp_volumefrom REAL, xrp_volumeto REAL, snapshot_added TEXT, snapshot_validity TEXT, FOREIGN KEY (date_idDate) REFERENCES date (idDate))')
    conn.commit()
    
def drop_dwh():
    c.execute("DROP TABLE XRP")
    c.execute("DROP TABLE aex")
    c.execute("DROP TABLE news")
    c.execute("DROP TABLE date")
    
if __name__ == "__main__":
    drop_dwh()
    create_dwh()